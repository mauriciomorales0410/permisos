<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('permisos', 'PermisoController');
Route::resource('salidas', 'SalidaController');
Route::get('capacitacion/{capacitacion}', 'PermisoController@showCap');
Route::get('viatico/{viatico}', 'PermisoController@showViatico');
Route::resource('viaticos', 'ViaticoController');

Route::get('/', function () {
    return view('Auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
